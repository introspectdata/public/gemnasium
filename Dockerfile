FROM node:11-alpine
RUN apk add --no-cache git
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
