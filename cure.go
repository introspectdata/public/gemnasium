package main

import (
	"context"
	"errors"
	"os"
	"os/exec"
	"path"
	"syscall"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
)

// errNotFileCurable is raised when the affected dependency file is not supported
type errFileNotCurable struct {
	filepath string
}

func (e errFileNotCurable) Error() string {
	return "Cannot cure unsupported dependency file: " + e.filepath
}

// Cure combines affections with their solution.
type Cure struct {
	Summary    string
	Diff       []byte
	Affections []scanner.Affection
}

// cureSource returns the cures for an affected dependency source.
func cureSource(ctx context.Context, source scanner.AffectedSource) ([]Cure, error) {
	// TODO implement plugins and plugin registry
	switch path.Base(source.FilePath) {
	case "yarn.lock":
		return cureYarnLock(ctx, source)
	}
	return nil, errFileNotCurable{source.FilePath}
}

var errAffectionNotCured = errors.New("Affection not cured")

// cureYarnLock cures a yarn.lock file.
func cureYarnLock(ctx context.Context, source scanner.AffectedSource) ([]Cure, error) {

	// get cure function of this source file and with cache
	var cureAffection = newCureYarnAffection(ctx, source)

	// iterate on source affections
	cures := []Cure{}
	for _, a := range source.Affections {
		cure, err := cureAffection(a, a.Dependency.Name)

		if isKilledErr(err) {
			return cures, context.DeadlineExceeded
		}

		switch err {
		case nil:
			cures = append(cures, *cure)
		case context.DeadlineExceeded:
			return cures, context.DeadlineExceeded
		case errAffectionNotCured:
			// ignore
		default:
			return nil, err
		}
	}
	return cures, nil
}

// CureFunc is the type of a function that cures an affection with an upgrade argument.
type CureFunc func(aff scanner.Affection, upgradeArg string) (*Cure, error)

// newCureYarnAffection generates a function that upgrades an affection in the context of a cure.
// It performs the depenencies upgrades using the given upgrade function.
func newCureYarnAffection(ctx context.Context, source scanner.AffectedSource) CureFunc {

	// create an upgrade function for given source file and context, add chaching
	var upgradeWithCache = newCachedUpgrade(func(arg string) (*UpgradeResult, error) {
		return upgradeYarn(ctx, source.AbsFilePath(), arg)
	})

	return func(aff scanner.Affection, upgradeArg string) (*Cure, error) {
		result, err := upgradeWithCache(upgradeArg)
		if err == errNoopUpgrade {
			return nil, errAffectionNotCured // no change thus not cured
		}
		if err != nil {
			return nil, err
		}

		// find updated dependency matching affected one
		if updated, found := findDep(aff.Dependency, result.Dependencies); !found {
			// dependency is gone so it's cured, proceed
		} else {
			if aff.Advisory.Affects(updated.Version) {
				return nil, errAffectionNotCured // not cured
			}
			// dependency is no longer affected, proceed
		}

		return &Cure{
			Summary:    "Upgrade " + upgradeArg,
			Diff:       result.Diff,
			Affections: []scanner.Affection{aff},
		}, nil
	}
}

// UpgradeFunc is the type of a function that upgrade dependencies using given upgrade argument.
type UpgradeFunc func(upgradeArg string) (*UpgradeResult, error)

// newCachedUpgrade adds caching to an upgrade function.
func newCachedUpgrade(upgrade UpgradeFunc) UpgradeFunc {

	// create a cache
	type cacheEntry struct {
		Result *UpgradeResult
		Error  error
	}
	var cache = make(map[string]cacheEntry)

	// wrap upgrade function and adds caching
	return func(arg string) (*UpgradeResult, error) {
		if entry, ok := cache[arg]; ok {
			return entry.Result, entry.Error
		}
		r, err := upgrade(arg)
		cache[arg] = cacheEntry{Result: r, Error: err}
		return r, err
	}
}

// UpgradeResult combines a yarn upgrade command with the resulting dependencies and git diff.
type UpgradeResult struct {
	Diff         []byte              // Diff is the git diff of the upgraded dependency files.
	Dependencies []parser.Dependency // Dependencies are the dependencies parsed in the upgraded dependency files.
}

var errNoopUpgrade = errors.New("Upgrade results in no change")

// upgradeYarn upgrades a yarn project, parses the dependencies,
// generates a diff, and restore the original files.
func upgradeYarn(ctx context.Context, yarnLockPath string, upgradeYarnArgs ...string) (*UpgradeResult, error) {
	result := &UpgradeResult{}

	// dependency files that are possibly modified
	files := []string{"yarn.lock", "package.json"}

	// cmd generates an external command.
	var dir = path.Dir(yarnLockPath)
	var cmd = func(name string, args []string) *exec.Cmd {
		var c = exec.CommandContext(ctx, name, args...)
		c.Dir = dir
		c.Env = os.Environ()
		c.Stderr = os.Stderr
		return c
	}

	// runCmd generates an external command to be started with Run().
	var runCmd = func(name string, args []string) *exec.Cmd {
		var c = cmd(name, args)
		c.Stdout = os.Stdout
		return c
	}

	// upgrade yarn project
	if err := runCmd("yarn", append([]string{"upgrade"}, upgradeYarnArgs...)).Run(); err != nil {
		return nil, err
	}

	// tell whether the files have changed
	if err := runCmd("git", append([]string{"diff", "-s", "--exit-code"}, files...)).Run(); err == nil {
		return nil, errNoopUpgrade
	}

	// get the diff
	diff, err := cmd("git", append([]string{"diff"}, files...)).Output()
	if err != nil {
		return nil, err
	}
	result.Diff = diff

	// parse updated yarn.lock
	f, err := os.Open(yarnLockPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	deps, err := yarn.Parse(f)
	if err != nil {
		return nil, err
	}
	result.Dependencies = deps

	// restore files
	if err := runCmd("git", append([]string{"checkout"}, files...)).Run(); err != nil {
		return nil, err
	}

	return result, nil
}

// isKilledErr tells whether the command returning the given error has been killed.
func isKilledErr(err error) bool {
	if e, ok := err.(*exec.ExitError); ok {
		if e.ProcessState.Sys().(syscall.WaitStatus).Signal() == syscall.SIGKILL {
			return true
		}
	}
	return false
}

// findDep finds a dependency in a list of dependency.
func findDep(d parser.Dependency, dd []parser.Dependency) (*parser.Dependency, bool) {
	for _, x := range dd {
		if d.Name == x.Name {
			return &x, true
		}
	}
	return nil, false
}
