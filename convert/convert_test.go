package convert

import (
	"bytes"
	"encoding/json"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gopkg.in/guregu/null.v3"
)

func TestConvert(t *testing.T) {

	// advisories
	pgAdvisory := scanner.Advisory{
		UUID:             "7d9ba955-fd99-4503-936e-f6833768f76e",
		Package:          scanner.Package{Type: "gem", Name: "pg"},
		AffectedVersions: []string{"0.8.0"},
		Identifier:       null.StringFrom("CVE-1234"),
		Title:            "Regular Expression Denial of Service",
		Description:      "Xyz is vulnerable to ReDoS in the Xyz parameter.",
		Solution:         null.StringFrom("Upgrade to latest version."),
		Links:            []string{"https://security.io/advisories/119", "https://security.io/advisories/117", "https://security.io/advisories/118"},
	}

	// analyzer output
	affected := []scanner.AffectedSource{
		{
			Source: scanner.Source{
				FilePath:       "rails/Gemfile.lock",
				PackageManager: issue.PackageManagerBundler,
				PackageType:    "gem",
				Deps: []parser.Dependency{
					{Name: "pg", Version: "0.8.0"},
					{Name: "puma", Version: "2.16.0"},
				},
			},
			Affections: []scanner.Affection{
				{
					Advisory:   pgAdvisory,
					Dependency: parser.Dependency{Name: "pg", Version: "0.8.0"},
				},
			},
		},
		{
			Source: scanner.Source{
				FilePath:       "node/yarn.lock",
				PackageManager: issue.PackageManagerYarn,
				PackageType:    "npm",
				Deps: []parser.Dependency{
					{Name: "acorn", Version: "4.0.4"},
					{Name: "acorn", Version: "3.3.0"},
					{Name: "acorn", Version: "4.0.11"},
					{Name: "@angular/animations", Version: "4.4.6"},
				},
			},
		},
	}

	// JSON input
	var input bytes.Buffer
	enc := json.NewEncoder(&input)
	enc.SetIndent("", "  ")
	if err := enc.Encode(affected); err != nil {
		t.Fatal(err)
	}

	// expected output
	prependPath := "app"
	category := issue.Category(issue.CategoryDependencyScanning)
	issueScanner := issue.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	want := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{
				Category:    category,
				Scanner:     issueScanner,
				Name:        "Regular Expression Denial of Service",
				Message:     "Regular Expression Denial of Service in pg",
				Description: "Xyz is vulnerable to ReDoS in the Xyz parameter.",
				CompareKey:  "app/rails/Gemfile.lock:pg:gemnasium:7d9ba955-fd99-4503-936e-f6833768f76e",
				Severity:    issue.SeverityLevelUnknown,
				Solution:    "Upgrade to latest version.",
				Location: issue.Location{
					File: "app/rails/Gemfile.lock",
					Dependency: issue.Dependency{
						Package: issue.Package{Name: "pg"},
						Version: "0.8.0",
					},
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "gemnasium",
						Name:  "Gemnasium-7d9ba955-fd99-4503-936e-f6833768f76e",
						Value: "7d9ba955-fd99-4503-936e-f6833768f76e",
						URL:   "https://deps.sec.gitlab.com/packages/gem/pg/versions/0.8.0/advisories",
					},
					{
						Type:  "cve",
						Name:  "CVE-1234",
						Value: "CVE-1234",
						URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1234",
					},
				},
				Links: []issue.Link{
					{URL: "https://security.io/advisories/117"},
					{URL: "https://security.io/advisories/118"},
					{URL: "https://security.io/advisories/119"},
				},
			},
		},
		Remediations: []issue.Remediation{},
		DependencyFiles: []issue.DependencyFile{
			{
				Path:           "app/rails/Gemfile.lock",
				PackageManager: "bundler",
				Dependencies: []issue.Dependency{
					{Package: issue.Package{Name: "pg"}, Version: "0.8.0"},
					{Package: issue.Package{Name: "puma"}, Version: "2.16.0"},
				},
			},
			{
				Path:           "app/node/yarn.lock",
				PackageManager: "yarn",
				Dependencies: []issue.Dependency{
					{Package: issue.Package{Name: "acorn"}, Version: "4.0.4"},
					{Package: issue.Package{Name: "acorn"}, Version: "3.3.0"},
					{Package: issue.Package{Name: "acorn"}, Version: "4.0.11"},
					{Package: issue.Package{Name: "@angular/animations"}, Version: "4.4.6"},
				},
			},
		},
	}

	// test
	r := bytes.NewReader(input.Bytes())
	got, err := Convert(r, prependPath)
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
