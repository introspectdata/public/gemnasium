package convert

import (
	"encoding/json"
	"io"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

const (
	scannerID   = "gemnasium"
	scannerName = "Gemnasium"

	identifierTypeGemnasiumUUID  = issue.IdentifierType("gemnasium")
	identifierPrefixGemnasiuUUID = "Gemnasium-"

	gemnasiumURL = "https://deps.sec.gitlab.com"
)

// Convert converts the output of the Gemnasium client (list of affected sources) to a report.
func Convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	var result []scanner.AffectedSource
	err := json.NewDecoder(reader).Decode(&result)
	if err != nil {
		return nil, err
	}
	return ToReport(result, prependPath), nil
}

// ToReport converts affected sources returned by the Gemnasium client to a report.
func ToReport(sources []scanner.AffectedSource, prependPath string) *issue.Report {
	issues := []issue.Issue{}
	depfiles := make([]issue.DependencyFile, len(sources))
	for i, source := range sources {
		issues = append(issues, toIssues(source, prependPath)...)
		depfiles[i] = toDependencyFile(source, prependPath)
	}
	report := issue.NewReport()
	report.Vulnerabilities = issues
	report.DependencyFiles = depfiles
	return &report
}

func toDependencyFile(source scanner.AffectedSource, prependPath string) issue.DependencyFile {
	return issue.DependencyFile{
		Path:           filepath.Join(prependPath, source.FilePath),
		Dependencies:   toDependencies(source.Deps),
		PackageManager: issue.PackageManager(source.PackageManager),
	}
}

func toDependencies(in []parser.Dependency) []issue.Dependency {
	out := make([]issue.Dependency, len(in))
	for i, d := range in {
		pkg := issue.Package{Name: d.Name}
		out[i] = issue.Dependency{Package: pkg, Version: d.Version}
	}
	return out
}

func toIssues(source scanner.AffectedSource, prependPath string) []issue.Issue {
	issues := make([]issue.Issue, len(source.Affections))
	for i, affection := range source.Affections {
		issues[i] = VulnerabilityConverter{
			PrependPath: prependPath,
			Source:      source,
			Advisory:    affection.Advisory,
			Dependency:  affection.Dependency,
		}.Issue()
	}
	return issues
}
