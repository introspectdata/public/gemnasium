package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	scannercli "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/cli"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/composer"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/mvnplugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/npm"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
)

const (
	flagTargetDir        = "target-dir"
	flagArtifactDir      = "artifact-dir"
	flagRemediate        = "remediate"
	flagRemediateTimeout = "remediate-timeout"

	defaultTimeoutRemediate = 5 * time.Minute
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "Gemnasium analyzer for GitLab Dependency-Scanning"
	app.Author = "GitLab"
	app.Commands = []cli.Command{runCommand()}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func runCommand() cli.Command {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:   flagTargetDir,
			Usage:  "Target directory",
			EnvVar: command.EnvVarTargetDir + "," + command.EnvVarCIProjectDir,
		},
		cli.StringFlag{
			Name:   flagArtifactDir,
			Usage:  "Artifact directory",
			EnvVar: command.EnvVarArtifactDir + "," + command.EnvVarCIProjectDir,
		},
		cli.BoolTFlag{
			Name:   flagRemediate,
			Usage:  "Remediate vulnerabilities",
			EnvVar: "DS_REMEDIATE",
		},
		cli.DurationFlag{
			Name:   flagRemediateTimeout,
			EnvVar: "DS_REMEDIATE_TIMEOUT",
			Usage:  "Time limit for vulnerabilities auto-remediation",
			Value:  defaultTimeoutRemediate,
		},
	}
	flags = append(flags, search.NewFlags()...)
	flags = append(flags, scannercli.ClientFlags()...)
	flags = append(flags, scannercli.FinderFlags()...)
	flags = append(flags, pathfilter.MakeFlags("DS_")...)

	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}

			// parse excluded paths
			filter, err := pathfilter.NewFilter(c)
			if err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flagTargetDir))
			if err != nil {
				return err
			}

			// search
			searchOpts := search.NewOptions(c)
			matchPath, err := search.New(plugin.Match, searchOpts).Run(targetDir)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, "Found project in "+matchPath)

			// find compatible files
			finder := scannercli.NewFinder(c)
			files, err := finder.FindFiles(targetDir)
			if err != nil {
				return err
			}

			// get sources of dependencies
			sources, err := scanner.ParseFiles(files)
			if err != nil {
				return err
			}

			// get advisories
			client, err := scannercli.NewClient(c)
			if err != nil {
				return err
			}
			advisories, err := client.Advisories(sources.Packages())
			if err != nil {
				return err
			}

			// convert to vulnerabilities and dependency files
			var affectedSources = scanner.AffectedSources(advisories, sources...)
			var prependPath = "" // empty because the analyzer scans the root directory
			var report = convert.ToReport(affectedSources, prependPath)

			// remediate vulnerabilities
			if c.BoolT(flagRemediate) {
				if !isGitClone(targetDir) {
					log.Println("auto-remediation requires a valid git directory") // don't fail
				} else {
					var t = c.Duration(flagRemediateTimeout)
					ctx, cancel := context.WithTimeout(context.Background(), t)
					defer cancel()
					report.Remediations = remediations(ctx, affectedSources...)
				}
			}

			// filter paths, sort
			report.ExcludePaths(filter.IsExcluded)
			report.Sort()

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), command.ArtifactNameDependencyScanning)
			f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f.Close()
			enc := json.NewEncoder(f)
			enc.SetIndent("", "  ")
			return enc.Encode(report)
		},
	}
}

// remediations attempts to cure affected sources and returns remediations.
func remediations(ctx context.Context, sources ...scanner.AffectedSource) []issue.Remediation {
	var allRems = make([]issue.Remediation, 0)
	for _, source := range sources {
		// attempt to cure
		cures, err := cureSource(ctx, source)
		switch err {
		case nil:
			// proceed
		case context.DeadlineExceeded:
			// report timeout and proceed
			log.Print("timeout exceeded during auto-remediation")
		default:
			// report error and move on to the next dependency file;
			// dependency scanning must not fail when remediation fails
			log.Print(err)
			continue
		}

		// convert cures to remediations;
		// it can't be extracted out of this loop because source is required.
		var rems = make([]issue.Remediation, len(cures))
		for i, cure := range cures {
			var refs = make([]issue.IssueRef, len(cure.Affections))
			for j, a := range cure.Affections {
				// HACK convert to an issue to get the exact compare key
				var ckey = convert.VulnerabilityConverter{
					Source:     source,
					Advisory:   a.Advisory,
					Dependency: a.Dependency,
				}.Issue().CompareKey

				refs[j] = issue.IssueRef{CompareKey: ckey}
			}
			rems[i] = issue.Remediation{
				Fixes:   refs,
				Summary: cure.Summary,
				Diff:    base64.StdEncoding.EncodeToString(cure.Diff),
			}
		}
		allRems = append(allRems, rems...)
	}
	return allRems
}

func isGitClone(dir string) bool {
	return exec.Command("git", "-C", dir, "status").Run() == nil
}
