package cli

import (
	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
)

const (
	flagIgnoredDirs   = flagPrefix + "ignore-dirs"
	envVarIgnoredDirs = envVarPrefix + "IGNORED_DIRS,SEARCH_IGNORED_DIRS"
)

var defaultIgnoredDirs = cli.StringSlice([]string{"node_modules", ".bundle", "vendor", ".git"})

// FinderFlags generates the command line flags that configures a Finder.
func FinderFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringSliceFlag{
			Name:   flagIgnoredDirs,
			EnvVar: envVarIgnoredDirs,
			Usage:  "Directory to be ignored",
			Value:  &defaultIgnoredDirs,
		},
	}
}

// NewFinder initializes a new Finder based on the flags of the command line context.
func NewFinder(c *cli.Context) *scanner.Finder {
	return &scanner.Finder{IgnoredDirs: c.StringSlice(flagIgnoredDirs)}
}
