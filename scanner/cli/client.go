package cli

import (
	"net/http"
	"net/url"
	"time"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
)

const (
	flagServerURL     = flagPrefix + "server-url"
	flagClientTimeout = flagPrefix + "client-timeout"

	envVarServerURL     = envVarPrefix + "SERVER_URL"
	envVarClientTimeout = envVarPrefix + "CLIENT_TIMEOUT"
)

// ClientFlags generates the command line flags that configures a Client.
func ClientFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   flagServerURL,
			EnvVar: envVarServerURL,
			Usage:  "Server URL",
			Value:  "https://deps.sec.gitlab.com/",
		},
		cli.StringFlag{
			Name:   flagClientTimeout,
			EnvVar: envVarClientTimeout,
			Usage:  "Client timeout",
			Value:  "1m",
		},
	}
}

// NewClient initializes a new Client based on the flags of the command line context.
func NewClient(c *cli.Context) (*scanner.Client, error) {
	// parse timeout
	timeout, err := time.ParseDuration(c.String(flagClientTimeout))
	if err != nil {
		return nil, err
	}

	// parse URL
	serverURL, err := url.Parse(c.String(flagServerURL))
	if err != nil {
		return nil, err
	}

	// new client
	cl := http.DefaultClient
	cl.Timeout = timeout
	return scanner.NewClient(cl, serverURL), nil
}
