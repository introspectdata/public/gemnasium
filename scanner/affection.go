package scanner

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"

// Affection combines an affected dependency with a security advisory.
type Affection struct {
	Dependency parser.Dependency `json:"dependency"`
	Advisory   Advisory          `json:"advisory"`
}

// AffectedSource combines a dependencies source with its affections.
type AffectedSource struct {
	Source
	Affections []Affection `json:"affections"`
}

// AffectedSources filters dependencies sources and return the affected ones.
func AffectedSources(advisories []Advisory, sources ...Source) []AffectedSource {
	result := []AffectedSource{}
	for _, source := range sources {
		affections := sourceAffections(source, advisories)
		result = append(result, AffectedSource{source, affections})
	}
	return result
}

func sourceAffections(s Source, advisories []Advisory) []Affection {
	out := []Affection{}
	for _, a := range advisories {
		if a.Package.Type == s.PackageType { // same package type?
			for _, d := range s.Deps {
				if d.Name == a.Package.Name { // same package name?
					if a.Affects(d.Version) { // affected version?
						out = append(out, Affection{Advisory: a, Dependency: d})
					}
				}
			}
		}
	}
	return out
}
