package scanner

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestAffectedSources(t *testing.T) {

	// advisories
	pgAdvisory := Advisory{
		Package:          Package{Type: "gem", Name: "pg"},
		AffectedVersions: []string{"0.8.0"},
	}

	pgAdvisory2 := Advisory{
		Package:          Package{Type: "gem", Name: "pg"},
		AffectedVersions: []string{"0.9.0", "0.8.0"},
	}

	acornAdvisory := Advisory{
		Package:          Package{Type: "npm", Name: "acorn"},
		AffectedVersions: []string{"4.0.4", "3.3.0"},
	}

	acornGemAdvisory := Advisory{
		Package:          Package{Type: "gem", Name: "acorn"},
		AffectedVersions: []string{"4.0.4", "3.3.0"},
	}

	advisories := []Advisory{pgAdvisory, pgAdvisory2, acornAdvisory, acornGemAdvisory}

	// sources
	gemfileLock := Source{
		FilePath:    "simple/Gemfile.lock",
		PackageType: "gem",
		Deps: []parser.Dependency{
			{Name: "pg", Version: "0.8.0"},
			{Name: "puma", Version: "2.16.0"},
		},
	}

	yarnLock := Source{
		FilePath:    "simple/yarn.lock",
		PackageType: "npm",
		Deps: []parser.Dependency{
			{Name: "acorn", Version: "4.0.4"},
			{Name: "acorn", Version: "3.3.0"},
			{Name: "acorn", Version: "4.0.11"},
			{Name: "@angular/animations", Version: "4.4.6"},
		},
	}

	sources := []Source{gemfileLock, yarnLock}

	// expectation
	want := []AffectedSource{
		{
			Source: gemfileLock,
			Affections: []Affection{
				{
					Advisory:   pgAdvisory,
					Dependency: parser.Dependency{Name: "pg", Version: "0.8.0"},
				},
				{
					Advisory:   pgAdvisory2,
					Dependency: parser.Dependency{Name: "pg", Version: "0.8.0"},
				},
			},
		},
		{
			Source: yarnLock,
			Affections: []Affection{
				{
					Advisory:   acornAdvisory,
					Dependency: parser.Dependency{Name: "acorn", Version: "4.0.4"},
				},
				{
					Advisory:   acornAdvisory,
					Dependency: parser.Dependency{Name: "acorn", Version: "3.3.0"},
				},
			},
		},
	}

	// test
	got := AffectedSources(advisories, sources...)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expecting:\n%v\nbut got:\n%v", want, got)
	}
}
