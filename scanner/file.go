package scanner

import (
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// File describes a dependency file supported by a parser.
// It is found by the Finder and parsed by ParseFiles.
type File struct {
	Path   string        // Path is relative to the root directory.
	Root   string        // Root is the directory where the finder has looked for files.
	Parser parser.Parser // Parser is the dependency file parser compatible with the file.
}

// Path is the absolute file path.
func (f File) AbsPath() string {
	return filepath.Join(f.Root, f.Path)
}
