package scanner

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"

// Package contains the information needed to resolve a package on the server side.
type Package struct {
	Type parser.PackageType `json:"type"`
	Name string             `json:"name"`
}
