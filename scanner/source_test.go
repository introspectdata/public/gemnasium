package scanner

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestSources(t *testing.T) {
	t.Run("Packages", func(t *testing.T) {
		// Sources.Package depends on Source.Package,
		// so both are tested at the same time.

		sources := Sources{
			{
				PackageType: "gem",
				Deps: []parser.Dependency{
					{Name: "pg", Version: "0.8.0"},
					{Name: "puma", Version: "2.16.0"},
				},
			},
			{
				PackageType: "npm",
				Deps: []parser.Dependency{
					{Name: "acorn", Version: "4.0.4"},
					{Name: "acorn", Version: "3.3.0"},
					{Name: "acorn", Version: "4.0.11"},
					{Name: "@angular/animations", Version: "4.4.6"},
				},
			},
		}

		want := []Package{
			{Name: "pg", Type: "gem"},
			{Name: "puma", Type: "gem"},
			{Name: "acorn", Type: "npm"},
			{Name: "acorn", Type: "npm"},
			{Name: "acorn", Type: "npm"},
			{Name: "@angular/animations", Type: "npm"},
		}

		got := sources.Packages()
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Wrong result. Expecting:\n%v\nbut got:\n%v", want, got)
		}
	})
}
