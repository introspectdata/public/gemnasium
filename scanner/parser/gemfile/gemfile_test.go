package gemfile

import (
	"encoding/json"
	"os"
	"reflect"
	"sort"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestGemfile(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		for _, tc := range []string{"simple", "big", "multi-sources"} {
			t.Run(tc, func(t *testing.T) {
				// Load fixture
				fixture, err := os.Open("fixtures/" + tc + "/Gemfile.lock")
				if err != nil {
					t.Error("Can't open fixture file", err)
				}
				defer fixture.Close()
				got, err := Parse(fixture)
				if err != nil {
					t.Fatal(err)
				}

				// Load expected output
				expect, err := os.Open("expect/" + tc + "/dependencies.json")
				if err != nil {
					t.Error("Can't open expect file", err)
				}
				defer expect.Close()
				var want []parser.Dependency
				err = json.NewDecoder(expect).Decode(&want)
				if err != nil {
					t.Fatal(err)
				}

				// Sort & Compare
				sortDependencies(got)
				sortDependencies(want)
				if !reflect.DeepEqual(got, want) {
					t.Errorf("Wrong result. Expected\n%v\nbut got\n%v", want, got)
				}
			})
		}
	})
}

func sortDependencies(deps []parser.Dependency) {
	sort.SliceStable(deps, func(i, j int) bool {
		if deps[i].Name != deps[j].Name {
			return deps[i].Name < deps[j].Name
		}
		return deps[i].Version < deps[j].Version
	})
}
