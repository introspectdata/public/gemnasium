package mvnplugin

import (
	"bytes"
	"encoding/json"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestMvnplugin(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		for _, tc := range []string{"simple", "big"} {
			t.Run(tc, func(t *testing.T) {
				fixturesPath := filepath.Join("fixtures", tc, "maven-dependencies.json")
				expectationPath := filepath.Join("expect", tc, "dependencies.json")

				// Load fixture
				fixture, err := os.Open(fixturesPath)
				if err != nil {
					t.Error("Can't open fixture file", err)
				}
				defer fixture.Close()

				// Parse & sort
				got, err := Parse(fixture)
				if err != nil {
					t.Fatal(err)
				}
				sortDependencies(got)

				// Look for expected output
				if _, err := os.Stat(expectationPath); err == nil {
					// Load expected output
					expect, err := os.Open(expectationPath)
					if err != nil {
						t.Error("Can't open expect file", err)
					}
					defer expect.Close()
					var want []parser.Dependency
					err = json.NewDecoder(expect).Decode(&want)
					if err != nil {
						t.Fatal(err)
					}

					// Compare
					if !reflect.DeepEqual(got, want) {
						t.Errorf("Wrong result. Expected\n%v\nbut got\n%v", want, got)
					}

				} else {
					// Make test fail
					t.Errorf("Creating expectation file: %s", expectationPath)

					// Create target directory if needed
					err := os.MkdirAll(filepath.Dir(expectationPath), 0755)
					if err != nil {
						t.Errorf("Cannot create dir: %s", err)
					}

					// Create missing file
					f, err := os.OpenFile(expectationPath, os.O_CREATE|os.O_WRONLY, 0644)
					if err != nil {
						t.Errorf("Cannot create expectation file: %s", err)
					}
					defer f.Close()
					b, err := json.Marshal(got)
					if err != nil {
						t.Fatal(err)
					}
					var out bytes.Buffer
					json.Indent(&out, b, "", "  ")
					out.WriteTo(f)
				}
			})
		}
	})
}

func sortDependencies(deps []parser.Dependency) {
	sort.SliceStable(deps, func(i, j int) bool {
		if deps[i].Name != deps[j].Name {
			return deps[i].Name < deps[j].Name
		}
		return deps[i].Version < deps[j].Version
	})
}
