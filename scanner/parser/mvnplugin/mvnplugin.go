package mvnplugin

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

type Dependency struct {
	GroupId    string `json:"groupId"`
	ArtifactId string `json:"artifactId"`
	Version    string `json:"version"` // Installed version
}

func Parse(r io.Reader) ([]parser.Dependency, error) {
	dependencies := []Dependency{}
	err := json.NewDecoder(r).Decode(&dependencies)
	if err != nil {
		return nil, err
	}
	pdeps := make([]parser.Dependency, len(dependencies))
	for i, dep := range dependencies {
		name := dep.GroupId + "/" + dep.ArtifactId
		pdeps[i] = parser.Dependency{Name: name, Version: dep.Version}
	}
	return pdeps, nil
}

func init() {
	parser.Register("mvnplugin", parser.Parser{
		Parse:          Parse,
		PackageManager: "maven",
		PackageType:    parser.PackageTypeMaven,
		Filenames:      []string{"maven-dependencies.json"},
	})
}
