package yarn

import (
	"bufio"
	"io"
	"regexp"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

type Document struct {
	Version      string       `json:"version"`
	Dependencies []Dependency `json:"dependencies"`
}

type Dependency struct {
	Name    string `json:"name"`
	Version string `json:"version"` // Installed version
	Line    int64  `json:"line"`
}

const supportedFileFormatVersion = "1"

var documentVersionRegexp *regexp.Regexp
var dependencySectionRegexp *regexp.Regexp
var dependencyNameRegexpSlit *regexp.Regexp
var dependencyLockedVersionRegexp *regexp.Regexp

func Parse(r io.Reader) ([]parser.Dependency, error) {
	document := Document{}
	document.Version = "1"
	document.Dependencies = []Dependency{}

	scanner := bufio.NewScanner(r)

	var line string
	var dep Dependency
	var lineNumber int64
	for scanner.Scan() {
		line = scanner.Text()
		lineNumber++

		switch {
		// Empty line
		case len(line) == 0:
			continue

		// Comment line
		case line[:1] == "#":
			// Look for file format version
			matches := documentVersionRegexp.FindStringSubmatch(line)
			if len(matches) == 0 {
				continue
			}
			document.Version = matches[1]

		// Dependency Section (first line)
		case dependencySectionRegexp.MatchString(line):
			// TODO: do this only once at beginning of parsing
			if document.Version != supportedFileFormatVersion {
				return nil, parser.ErrWrongFileFormatVersion
			}
			matches := dependencySectionRegexp.FindStringSubmatch(line)
			dep.Name = strings.Trim(dependencyNameRegexpSlit.Split(matches[1], -1)[0], `"`)
			dep.Line = lineNumber

		// Dependency Section (line showing locked version)
		case dependencyLockedVersionRegexp.MatchString(line):
			matches := dependencyLockedVersionRegexp.FindStringSubmatch(line)
			dep.Version = strings.Trim(matches[1], `"`)
			document.Dependencies = append(document.Dependencies, dep)
			dep = Dependency{}
		}
	}

	pdeps := make([]parser.Dependency, len(document.Dependencies))
	for i, dep := range document.Dependencies {
		pdeps[i] = parser.Dependency{Name: dep.Name, Version: dep.Version}
	}
	return pdeps, nil
}

func init() {
	// register parser
	parser.Register("yarn", parser.Parser{
		Parse:          Parse,
		PackageManager: "yarn",
		PackageType:    parser.PackageTypeNpm,
		Filenames:      []string{"yarn.lock"},
	})

	// compile regexps
	documentVersionRegexp = regexp.MustCompile(`\# yarn lockfile v(\d)`)
	dependencySectionRegexp = regexp.MustCompile(`^(\S.*):$`)
	dependencyNameRegexpSlit = regexp.MustCompile(`\b@`)
	dependencyLockedVersionRegexp = regexp.MustCompile(`^\s+version\s+"(\S+)"`)
}
