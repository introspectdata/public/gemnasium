package scanner

import (
	"os"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/yarn"
)

func TestParse(t *testing.T) {
	f, err := os.Open("parser/gemfile/fixtures/simple/Gemfile.lock")
	if err != nil {
		t.Fatal(err)
	}
	defer f.Close()

	got, err := Parse(f, "Gemfile.lock", "xyz")
	if err != nil {
		t.Fatal(err)
	}

	want := &Source{
		FilePath:       "xyz",
		PackageType:    "gem",
		PackageManager: "bundler",
		Deps: []parser.Dependency{
			{Name: "pg", Version: "1.0.0"},
			{Name: "puma", Version: "2.16.0"},
		},
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expecting:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestParseFiles(t *testing.T) {
	getParser := func(filename string) parser.Parser {
		if p := parser.Lookup(filename); p != nil {
			return *p
		}
		t.Fatal(ErrParserNotFound{filename})
		return parser.Parser{}
	}

	files := []File{
		{
			Path:   "simple/Gemfile.lock",
			Root:   "parser/gemfile/fixtures",
			Parser: getParser("Gemfile.lock"),
		},
		{
			Path:   "simple/yarn.lock",
			Root:   "parser/yarn/fixtures",
			Parser: getParser("yarn.lock"),
		},
	}

	got, err := ParseFiles(files)
	if err != nil {
		t.Fatal(err)
	}

	want := Sources{
		{
			FilePath:       "simple/Gemfile.lock",
			RootDir:        "parser/gemfile/fixtures",
			PackageType:    "gem",
			PackageManager: "bundler",
			Deps: []parser.Dependency{
				{Name: "pg", Version: "1.0.0"},
				{Name: "puma", Version: "2.16.0"},
			},
		},
		{
			FilePath:       "simple/yarn.lock",
			RootDir:        "parser/yarn/fixtures",
			PackageType:    "npm",
			PackageManager: "yarn",
			Deps: []parser.Dependency{
				{Name: "acorn", Version: "4.0.4"},
				{Name: "acorn", Version: "3.3.0"},
				{Name: "acorn", Version: "4.0.11"},
				{Name: "@angular/animations", Version: "4.4.6"},
			},
		},
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expecting:\n%#v\nbut got:\n%#v", want, got)
	}
}
