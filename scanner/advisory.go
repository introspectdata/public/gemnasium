package scanner

import "gopkg.in/guregu/null.v3"

// Advisory is a security advisory published for a package.
type Advisory struct {
	UUID             string      `json:"uuid"`              // UUID is a unique identifier string.
	Category         null.String `json:"category"`          // Category is either "critical" or "security". It's deprecated.
	Identifier       null.String `json:"identifier"`        // Identifier is the CVE id (preferred) or any public identifier.
	Title            string      `json:"title"`             // Title is a short description of the security flaw.
	Description      string      `json:"description"`       // Description is a long description of the security flaw and the possible risks.
	DisclosureDate   string      `json:"date"`              // DisclosureDate is the date on which the advisory was made public.
	FixedVersions    []string    `json:"fixed_versions"`    // FixedVersions are the versions fixing the vulnerability.
	AffectedVersions []string    `json:"affected_versions"` // AffectedVersions are the versions affected by the vulnerability.
	Solution         null.String `json:"solution"`          // Solution describes how to remediate the vulnerability.
	Credit           null.String `json:"credit"`            // Credit gives the names of the people who reported the vulnerability or helped fixing it.
	Links            []string    `json:"urls"`              // Links are URLs of: detailed advisory, documented exploit, vulnerable source code, etc.
	Package          Package     `json:"package"`           // Package is a reference to the affected package (package type and name).
}

// Affects tells whether a version is affected.
func (a Advisory) Affects(number string) bool {
	for _, v := range a.AffectedVersions {
		if v == number { // TODO compare using semver
			return true
		}
	}
	return false
}
