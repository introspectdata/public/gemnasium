# Gemnasium analyzer changelog

## v2.2.4
- Fix `DS_EXCLUDED_PATHS` not applied to dependency files (!18)

## v2.2.3
- Fix dependency list, include dependency files which do not have any vulnerabilities (!17)

## v2.2.2
- Fix npm-shrinkwrap.json files not parsed (!12)

## v2.2.1
- Sort the dependency files and their dependencies (!14)
- Fix vulnerabilities not sorted in report (!14)
- Fix missing `DS_EXCLUDED_PATH` variable (!14)

## v2.2.0
- List the dependency files and their dependencies (!13)

## v2.1.2
- Update common to v2.1.6

## v2.1.1
- Sort vulnerability.links to ensure stable order

## v2.1.0
- Implement vulnerabilities remediation for yarn

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
